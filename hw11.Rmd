---
title: "Parties and Taxis"
author: "Maksym Melnyk, Evgen Dorodnikov"
#output: pdf_document
output: 
  pdf_document:
    latex_engine: xelatex
urlcolor: blue
---

# Exercise 1

## Identifying your business goals

### Background

New York is one of the biggest cities in the world, and that's why there are a lot of all kinds of parties there. Since parties involve drinking alcohol, by law, participants do not have the right to go home by driving. In this case, if the house is not located close to the venue, the taxi service comes to the rescue.
The taxi service responds to calls by the situation, since they can not predict where or in which area the party will take place. The number of calls from one point, in this case, can be very high. Our goal is to help the taxi service to settle the placement of cars given this figure. And as to find suitable places for parties, that participants would not have problems with the police.

### Business goals

1) Increase taxi service's inclome.  
2) Decrease alcohol car crashing incidents.  
3) Decrease police calls because of parties.  

### Business success criteria

Our criterias are, basically, **income of taxi service**, **number of car crashing incidents** and **number of calls to police**. Meaning increasing or decreasing these measures.  

## Assessing your situation

### Inventory of resources

1) Data miners (Maksym Melnyk, Evgen Dorodnikov)  
2) Dataset "Parties in New York"  
3) Dataset "New York City Taxi"  
4) Software "R", "RStudio"  
5) 16gb RAM, 8-core 2.4GHz  

### Requirements, assumptions, and constraints

1) At least 25 hours of work per member  
2) Every team must insert the project’s title, description and team members into the List of Projects document  
3) Every team must present the project as a poster in the poster session on Jan 8, 2018  
4) Every team must provide access for the grading instructor to the project code repository hosted either at Github or BitBucket page  
5) The project will be graded by the instructors and will get maximally 20 points  
6) If project gets X points then each team member gets X points  
7) Getting at least 10 points for the project is a prerequisite

### Risks and contingencies

1) Illness of one of the participants. The second participant should spend more time.  
2) No internet connection. Work without internet as possible.  
3) Computer failure. Ask UT about a computer.  
4) No working place. Ask UT about working place.  

### Terminology

1) "Taxi data" - the dataset "New York City Taxi", which we use for the project.  
2) "Parties data" - the dataset "Parties in New York", which we use for the project.  
3) "Police calls" - the number of police calls about one of the party.  
4) "Cars" - number of taxi cars.  
5) "Area" - one of the New York considering districts.  

### Costs and benefits

1) Increasing taxi service income for 500000$  
2) Decreasing alcohol car crashing incidents for 20%  
3) Decreasing police calls because of parties for 30%  

## Defining your data-mining goals

### Data-mining goals

1) Map of the parties locations.  
2) Map of the taxi calls.  
3) Combined map of 1 and 2.  
4) Predicting model of the police calls depending on a location.  
5) Predicting model of the taxi pickup time depending on a location.  

### Data-mining success criteria

For 1-3 data-mining goals, the instructor (Meelis Kull) will decide how good our map or plot.  
For 4-5 data-mining goals, criteria is accuracy at least 85%.  

\newpage

# Exercise 2

##Gathering data

###Outline data requirements

Through our research we will need next data:

      * Distance between 2 destinations and overall travel time. 
      Time range : after 2015 year. 
      Data fromat: CSV.
      * Data about noise complaints in ~500 meters radius from party. 
      Time range : at least after 2015 year. 
      Data fromat: CSV.
      * Data about car accidents. 
      Tame range: after 2015. 
      Data format: CSV
      
###Verify data availability

Data                                   Availability     Satisfies requirements
------------------------------------ ---------------- -------------------------
Routes between streets in New York    Available         Yes
Noice complains in New York           Available         Yes
Car accidents                         Available         Yes

###Define selection criteria

For this research we will use some datasets which we found on Kaggle.

1. New York City Taxi dataset. Tables: 
  * Fastest routes. Fields:
      * Starting street.
      * End street.
      * Total distance.
      * Total travel time.
  * Second fastest routes. Fields:
      * Starting street.
      * End street.
      * Total distance.
      * Total travel time.
  * Accidents. Fields:
      * Borough.
      * Zip code.
      * Latitude.
      * Longitude.
      * Location.
      * On street name.
      * Cross street name.
      * Off street name.
      * Date of crush.
    
2. 2016 Parties in New York dataset. Tables:
  * Party locations. Ranges: Only where City == New York. Fields:
      * Location type.
      * Incident zip.
      * City.
      * Borough.
      * Latitude.
      * Longitude.
      * Number of calls.
  * Complains. Ranges: Only where City == New York. Fields:
      * Create date.
      * Close date.
      * Location type.
      * Incident zip.
      * City.
      * Borough.
      * Latitude.
      * Longitude.
    
##Describing data

The source of data for our project is dataset from Kaggle. All data is free to use and organised in tables which are available in .CSV formats.
If more specifically, to achieve our goals we will use two datasets. One is for taxi routes and accidents and second one for parties in New York.

###First dataset: New York City Taxi with OSRM.

Link: https://www.kaggle.com/oscarleo/new-york-city-taxi-with-osrm/data

Total amount of tables: 8.

Needed tables: 4.

1-3. fastest_routes_train_part_1.csv, fastest_routes_train_part_2.csv and second_fastest_routes_train.csv - this tables contain information of same type: streets and destination, time between them. Also they contain detailed description of route, but we no need it.

Fileds:

* id
* starting_street - place where driver picks up customer. Type: string.
* end_street - place where driver drops customer off. Type: string.
* total_distance - driving distance in meters. Type: numeric.
* total_travel_time - travel time in seconds. Type: numeric.

4. accidents_2016.csv - table contains data about car crashes (most likely taxis) that happed in New York, in 2016.

Fields:

* BOROUGH - borough in which accident happened. Type: string.
* ZIP CODE - postal code of certain borough. Type: numeric.
* LATITUDE - latitude of point which represents accident on map. Type: numeric.
* LONGITUDE - longtitude of point which represents accident on map. Type: numeric.
* LOCATION - latitude and longtitude of point which represents accident on map. Type: string.
* ON STREET NAME - name of street on which happened accident. Type: string.
* CROSS STREET NAME - name of cross street where happened accident. Type: string.
* OFF STREET NAME - name of street near which happened accident. Type: string.
* datetime - date and time of accident. Type: string.

###Second dataset: 2016 Parties in New York.

Link: https://www.kaggle.com/somesnm/partynyc/data

Total amount of tables: 4.

Needed tables: 2.

1. bar_locations.csv - table that contain information about parties with at least 10 calls in last 5 years.

Fields:

* Location Type - type of location, where party happened. Type: string.
* Incident Zip - zip code of place. Type: numeric.
* City - city where party happened. Type: string.
* Borough - borough in which party happened. Type: string.
* Latitude - latitude of point that represents place where party was carried out. Type: numeric.
* Longitude - longitude of point that represents place where party was carried out. Type: numeric.
* num_calls - total number of reports to police about certain party. Type: numeric.

2. party_in_nyc.csv - table that contains data about reports.

Fields:

* Created Date - date when party started. Type: string.
* Closed Date - date when party was closed. Type: string.
* Location Type - type of location where party happened. Type: string.
* Incident Zip - zip code of place. Type: numeric.
* City - city where party happened. Type: string.
* Borough - borough where was party. Type: string.
* Latitude - latitude of point that represents place where party was held. Type: numeric.
* Longitude - longitude of point that represents place from where call happened. Type: numeric.

Although tables contain unnecessary data, we assume that acquired information will be enough to complete your goals.


##Exploring data

###First dataset: New York City Taxi with OSRM.

###_Table "fastest_routes_train_part_1.csv"_

```{r}
fst_routes <- read.csv('fastest_routes_train_part_1.csv', header=TRUE)
```

Number of rows:
```{r}
nrow(fst_routes)
```

**Field: starting_street**

Type: string

Values:
```{r, message=F, warning=F}
#unique(fst_routes$starting_street)

# 2000+ different streets
```

Empty values:
```{r}
nrow(fst_routes[fst_routes$starting_street == "",])
```

Values count:
```{r, message=F, warning=F}
#table(fst_routes$starting_street)

# All 2000+ values are unique. No dublicates.
```

**Field: end_street**

Type: string

Values:
```{r, message=F, warning=F}
#unique(fst_routes$end_street)

# 2000+ different streets
```

Empty values:
```{r}
nrow(fst_routes[fst_routes$end_street == "",])
```

Values count:
```{r, message=F, warning=F}
#table(fst_routes$end_street)

# All 2000+ values are unique. No dublicates.
```

**Field: total_distance**

Type: numeric

Values:
```{r, message=F, warning=F}
#unique(fst_routes$total_distance)

# Different value for each row.
```

Empty values:
```{r}
nrow(fst_routes[fst_routes$total_distance == 0,])
```

Values count:
```{r, message=F, warning=F}
#table(fst_routes$total_distance)

# Different value almost for each row.
```

**Field: total_travel_time**

Type: numeric

Values:
```{r, message=F, warning=F}
#unique(fst_routes$total_travel_time)

# Different value for each row.
```

Empty values:
```{r}
nrow(fst_routes[fst_routes$total_travel_time == 0,])
```

Values count:
```{r, message=F, warning=F}
#table(fst_routes$total_travel_time)

# Different value almost for each row.
```

Tables fastest_routes_train_part_2.csv and second_fastest_routes_train.csv are contains the same columns as fastest_routes_train_part_1.csv and so the same type of data. So no need for exploring them.




###_Table "fastest_routes_train_part_1.csv"_

```{r}
accidents <- read.csv('accidents_2016.csv', header=TRUE)
```

Number of rows:
```{r}
nrow(accidents)
```

**Field: BOROUGH**

Type: string

Values:
```{r}
unique(accidents$BOROUGH)
```

Empty values:
```{r}
nrow(accidents[accidents$BOROUGH == "",])
```

Values count:
```{r}
table(accidents$BOROUGH)
```

**Field: ZIP CODE**

Type: numeric

Values:
```{r}
#unique(accidents$ZIP.CODE)

# 200- different values.
```

Empty values:
```{r}
nrow(accidents[accidents$ZIP.CODE == 0,])
```

Values count:
```{r}
#table(accidents$ZIP.CODE)

# 200- different values.
```

**Field: LATITUDE**

Type: numeric

Values:
```{r, message=F, warning=F}
#unique(accidents$LATITUDE)

# Different value almost for each row.
```

Empty values:
```{r}
nrow(accidents[accidents$LATITUDE == 0,])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$LATITUDE)

# Different value almost for each row.
```

**Field: LONGITUDE**

Type: numeric

Values:
```{r, message=F, warning=F}
#unique(accidents$LONGITUDE)

# Different value almost for each row.
```

Empty values:
```{r}
nrow(accidents[accidents$LONGITUDE == 0,])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$LONGITUDE)

# Different value almost for each row.
```

**Field: LOCATION**

Type: string

Values:
```{r, message=F, warning=F}
#unique(accidents$LOCATION)

# Different value almost for each row.
```

Empty values:
```{r}
nrow(accidents[accidents$LOCATION == "",])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$LOCATION)

# Different value almost for each row.
```

**Field: ON STREET NAME**

Type: string

Values:
```{r, message=F, warning=F}
#unique(accidents$ON.STREET.NAME)

# 5000- values
```

Empty values:
```{r}
nrow(accidents[accidents$ON.STREET.NAME == "",])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$ON.STREET.NAME)

# 5000- values
```

**Field: CROSS STREET NAME**

Type: string

Values:
```{r, message=F, warning=F}
#unique(accidents$CROSS.STREET.NAME)

# 6000- values
```

Empty values:
```{r}
nrow(accidents[accidents$CROSS.STREET.NAME == "",])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$CROSS.STREET.NAME)

# 6000- values
```

**Field: OFF STREET NAME**

Type: string

Values:
```{r, message=F, warning=F}
#unique(accidents$OFF.STREET.NAME)

# 16000- values
```

Empty values:
```{r}
nrow(accidents[accidents$OFF.STREET.NAME == "",])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$OFF.STREET.NAME)

# 16000- values
```

**Field: datetime**

Type: string

Values:
```{r, message=F, warning=F}
#unique(accidents$datetime)

# 54000- values
```

Empty values:
```{r}
nrow(accidents[accidents$datetime == "",])
```

Values count:
```{r, message=F, warning=F}
#table(accidents$datetime)

# 54000- values
```

###Second dataset: 2016 Parties in New York.
\newpage

!["Parties locations. Analysis of table.png"](bar_locations.png)

!["Parties. Analysis of table.png"](party_in_nyc.png)

\newpage

###Verifying data quality

Detailed data examination showed that datasets fulfill the most of requirements and can be used to reach our goals. Ofcourse there are some problems, like too much null values or lack of data in some tables, but they are not criticatl and may be solved using data maining techniques. No major quality issues were found. 

\newpage

# Exercise 3

## Plan ([Task Management](https://trello.com/b/XP9qSKVI/parties-and-taxis))

1) Data understanding (1h per each member)  
2) Data preparation (3h per each member)  
3) Data Filtering (3h per each member)  
4) Plot parties (5h for Maksym Melnyk)  
5) Plot taxis (5h for Evgen Dorodnikov)  
6) Build Taxis model (10h for Evgen Dorodnikov)  
7) Build Parties model (10h for Maksym Melnyk)  
8) Composition plot (3h per each member)  


















